<?php
namespace Index\Controller;

use Think\Controller;

Vendor('jpush.autoload');

use JPush\Client as JPushClient;

class JpushController extends Controller
{
	private $app_key;

	private $app_secret;

	public $client;

	// 构造函数
	public function __construct()
	{
		$this->app_key    = C('JPUSH.APP_KEY');

		$this->app_secret = C('JPUSH.MASTER_SECRET');

		$this->client     = new JPushClient($this->app_key, $this->app_secret);
	}

	/**
	 *  向所有用户app发送消息 
	 *  @param string $message 推送内容
	 */
	public function sendAll($message)
	{
		$result = $this->client
				  	   ->push()
				       ->setPlatform('all')
					   ->iosNotification($message, array(
											'sound' => 'sound.caf',
											'badge' => '+1',
											'content-available' => true,
											'mutable-content' => true,
											'extras' => array(
												'type' => 1,
											),
									))
				       ->addAllAudience()
				       // ->setNotificationAlert($message)
					   ->send();
		return json_encode($result);
	}

	/**
	 * 向指定用户发送消息 
	 * @param array $mobile_id 用户设备id(可是多个 array表示)
     * @param string $message  推送的消息
	 */
	public function sendSpecialSingle($mobile_id,$message)
	{	
		$result = $this->client
					   ->push()
					   ->setPlatform(['ios'])
					   ->addRegistrationId($mobile_id)
					   // ->addAlias('alias')
					   // ->addTag(array('tag1', 'tag2'))
					   ->iosNotification($message, array(
													 'sound' => 'sound.caf',
													 'badge' => '+1',
													 'content-available' => true,
													 'mutable-content' => true,
													 'extras' => array(
														'type' => 1,
													),
												))
					   // ->setNotificationAlert($message)
					   ->options(['sendno'=>1000,'time_to_live'=>1,'apns_production'=>false,'big_push_duration'=>1])
					   ->send();
		return json_encode($result);
	}
	
	/**
     * 向指定设备推送自定义消息
     * @param string $message 发送消息内容
     * @param array $regid 特定设备的id
     * @param string $title 标题
     */
    public function sendCustomMsg($regid,$message)
	{
		$result = $this->client
						->push()
						->setPlatform('all')
						->addRegistrationId($regid)
						// ->addAllAudience()
						->message($message, [
								  'title' => 'Hello',
								  'content_type' => 'text',
								  'extras' => [
								   'type'  => 0
								  ]
								])
						->options(array(
								"apns_production" => false
						))
						->send();
		return json_encode($result);
    }
	/**
	 * 定时推送消息 
	 * @param string $message 消息
	 * @param string $time 定时时间
	 */ 
	public function sendTimingInfo($message,$time)
	{
		$model_load = $this->client
						->push()
					    ->setPlatform("all")
					    ->addAllAudience()
					    ->setNotificationAlert($message)
					    ->build();
		$result = $this->client
					   ->schedule()
					   ->createSingleSchedule("每天14点发送的定时任务", $model_load, ['time'=>$time]);
		return  json_encode($result);
	}

	/**
	 * 每天定时推送消息 （时间段）
	 * @param string $message    消息 
	 * @param string $start_time 开始时间 (例如2017-09-04 15:40:58)
	 * @param string $end_time   结束时间 (例如2017-09-24 15:40:58)
	 * @param string $time       投放时间 (例如15:35:25)
	 * @param string $time_type  消息类型 
	 */
	public function sendEveryDayInfo($message,$start_time,$end_time,$time,$time_type = 'DAY')
	{
		$model_load = $this->client
						->push()
					    ->setPlatform("all")
					    ->addAllAudience()
					    ->setNotificationAlert($message)
					    ->build();
		$result = $this->client
					   ->schedule()
					   ->createSingleSchedule("每天14点发送的定时任务", $model_load, ['start'=> $start_time,'end'=> $end_time,'time'=> $time,'time_unit'=> $time_type,"frequency"=>1]);
		return  json_encode($result);
	}

	/**
	 * 数据统计 
	 * @param array $msgIds 推送消息返回的msg_id列表(数组形式或者字符串),默认数组
	 */
	public function getDataNotify($msgIds)
	{
		$result = $this->client->report()->getReceived($msgIds);

		return  json_encode($result);
	}

	/**
	 * 获取发送的用户
	 * @param string $time_unit  时间单位('HOUR', 'DAY', 'MONTH')
	 * @param array $start       开始时间  
	 * @param array $duration    时间段
	 */ 
	public function getUsers($time_unit, $start, $duration)
	{
		$result = $this->client->report()->getUsers($time_unit, $start, $duration);

		return  json_encode($result);
	}

	/**
	 * 获取设备型号 
	 * @param string $registrationId  设备id号
	 */
	public function getDeviceModel($registrationId)
	{
		$result = $this->client->device()->getDevices($registrationId);

		return  json_encode($result);
	}

	/**
	 * 添加标签（为区分用户类别）
	 * @param string $registration_id  设备id号
	 * @param array $tags      标签名
	 */
	public function AddDeviceTags($registration_id, $tags)
	{
		$result = $this->client->device()->addTags($registration_id, $tags);

		return  json_encode($result);
	}

	/**
	 * 删除标签
	 * @param string $registration_id  设备id号
	 * @param array $tags  标签名
	 */
	public function deleteDeviceTags($registration_id,$tags)
	{
		$result = $this->client->device()->removeTags($registration_id, $tags);

		return  json_encode($result);
	}

	/**
	 * 更新设备
	 * @param string $registrationId  设备id号
	 * @param array $alias   设备别名
	 * @param array $mobile  手机号
	 * @param array $addTags 添加的标签
	 * @param array $removeTags  移除的标签
	 */
	public function updateDeviceInfo($registrationId, $alias = null, $mobile = null, $addTags = null, $removeTags = null)
	{
		$result = $this->client->device()->updateDevice($registrationId, $alias, $mobile, $addTags, $removeTags);

		return  json_encode($result);
	}
	
}
